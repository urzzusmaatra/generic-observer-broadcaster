﻿using System;
using System.Collections.Generic;
using Broadcaster.Channel;
namespace Broadcaster
{
    public class DataBroadcasterService : IDataBroadcasterService
    {
        private Dictionary<Object, List<IChannel>> _broadcasters;
        
        public DataBroadcasterService()
        {
            this._broadcasters = new Dictionary<Object, List<IChannel>>();
        }

        public void CreateChannel<T, B>(T broadcaster) where T : new()
        {
            if (this._broadcasters.ContainsKey(broadcaster))
            {
                this._broadcasters[broadcaster].Add(new Channel<B>());
            }
            else
            {
                this._broadcasters.Add(broadcaster, new List<IChannel>() { new Channel<B>() });
            }
        }

        public bool TryJoinChannel<T, B>(Action<B> action)
        {
            var anySuccess = false;
            foreach (var channels in this._broadcasters)
            {
                if(channels.Key is T)
                {
                    foreach (var item in this._broadcasters[channels.Key])
                    {
                        var broadcaster = item.GetBroadcaster<B>();
                        if(broadcaster != null)
                        {
                            broadcaster.AddBroadcastListener(action);
                            anySuccess = true;
                        }
                    }
                }
            }
            return anySuccess;
        }

        public void LeaveChannel<T, B>(Action<B> action)
        {
            foreach (var channels in this._broadcasters)
            {
                if (channels.Key is T)
                {
                    foreach (var item in this._broadcasters[channels.Key])
                    {
                        var broadcaster = item.GetBroadcaster<B>();
                        if (broadcaster != null)
                        {
                            broadcaster.RemoveBroadcastListener(action);
                        }
                    }
                }
            }
        }

        public bool ContainsChannel<T>()
        {
            foreach (var key in this._broadcasters.Keys)
            {
                if (key is T)
                {
                    return true;
                }
            }
            return false;
        }

        public void Invoke<T>(Object invoker, T data)
        {
            if(this._broadcasters.TryGetValue(invoker, out List<IChannel> broadcasters))
            {
                foreach (var broadcaster in broadcasters)
                {
                    broadcaster.GetBroadcaster<T>()?.Invoke(data);
                }
            }
        }
    }
}
