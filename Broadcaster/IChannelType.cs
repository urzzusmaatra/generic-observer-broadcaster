﻿namespace Broadcaster.Channel
{
    public interface IChannel
    {
        IChannelType<T> GetBroadcaster<T>();
    }
}
