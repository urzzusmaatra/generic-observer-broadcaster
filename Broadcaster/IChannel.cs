﻿using System;

namespace Broadcaster.Channel
{
    public interface IChannelType<T>
    {
        void AddBroadcastListener(Action<T> listener);
        void RemoveBroadcastListener(Action<T> listener);
        void Invoke(T data);
    }
}
