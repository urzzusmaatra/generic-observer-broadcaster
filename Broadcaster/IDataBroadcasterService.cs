﻿using System;

namespace Broadcaster
{
    public interface IDataBroadcasterService
    {
        bool ContainsChannel<T>();
        void CreateChannel<T, B>(T broadcaster) where T : new();
        bool TryJoinChannel<T, B>(Action<B> action);
        void LeaveChannel<T, B>(Action<B> action);
        void Invoke<T>(Object invoker, T data);
    }
}