﻿using System;

namespace Broadcaster.Channel
{
    public class Channel<T> : IChannel, IChannelType<T>
    {

        private Action<T> _actions;

        public void AddBroadcastListener(Action<T> listener)
        {
            _actions += listener;
        }

        public void RemoveBroadcastListener(Action<T> listener)
        {
            _actions -= listener;
        }

        public IChannelType<T> GetBroadcaster<T>()
        {
            return (this is IChannelType<T>) ? (this as IChannelType<T>) : null;
        }

        public void Invoke(T data)
        {
            this._actions?.Invoke(data);
        }
    }
}
