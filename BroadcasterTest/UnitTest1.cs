using NUnit.Framework;
using Broadcaster;

namespace BroadcasterTest
{
    public class Tests
    {
        private IDataBroadcasterService broadcasterService;
        private DummyBroadcaster1 firstBroadcaster;
        private class DummyBroadcaster1 { }

        private class DummyChannel2 { }

        private class NOChannel { }

        public class Data
        {
            public float x, y, z;
        }

        [SetUp]
        public void Setup()
        {
            this.broadcasterService = new DataBroadcasterService();
            this.firstBroadcaster = new DummyBroadcaster1();
        }

        [Test]
        public void CreateChannel()
        {
            Assert.IsFalse(this.broadcasterService.ContainsChannel<DummyBroadcaster1>());
            this.broadcasterService.CreateChannel<DummyBroadcaster1, Data>(this.firstBroadcaster);
            Assert.IsTrue(this.broadcasterService.ContainsChannel<DummyBroadcaster1>());
        }

        [Test]
        public void NonExistentChannelJoin()
        {
            Assert.IsFalse(this.broadcasterService.TryJoinChannel<DummyChannel2, Data>(RandomListener));
        }

        [Test]
        public void JoinChannel()
        {
            this.broadcasterService.CreateChannel<DummyBroadcaster1, Data>(this.firstBroadcaster);
            Assert.IsTrue(this.broadcasterService.TryJoinChannel<DummyBroadcaster1, Data>(RandomListener));
        }

        [Test]
        public void InvokeListeners()
        {
            this.broadcasterService.CreateChannel<DummyBroadcaster1, Data>(this.firstBroadcaster);
            this.broadcasterService.TryJoinChannel<DummyBroadcaster1, Data>(DataListener);

            startData = new Data() { x = 1f, y = 1f, z = 1f };
            var data = new Data() { x = 2f, y = 2f, z = 2f };
            this.broadcasterService.Invoke<Data>(this.firstBroadcaster, data);
            Assert.AreEqual(startData, data);
        }

        [Test]
        public void InvokeMultipleListeners()
        {
            this.broadcasterService.CreateChannel<DummyBroadcaster1, Data>(this.firstBroadcaster);

            this.broadcasterService.TryJoinChannel<DummyBroadcaster1, Data>(DataListener);
            this.broadcasterService.TryJoinChannel<DummyBroadcaster1, Data>(OtherDataListener);

            this.broadcasterService.CreateChannel<DummyBroadcaster1, int>(this.firstBroadcaster);

            this.broadcasterService.TryJoinChannel<DummyBroadcaster1, int>(AddListener);

            startData = new Data() { x = 1f, y = 1f, z = 1f };
            startData2 = new Data() { x = 4f, y = 4f, z = 4f };
            var data = new Data() { x = 2f, y = 2f, z = 2f };
            this.broadcasterService.Invoke<Data>(this.firstBroadcaster, data);
            Assert.AreEqual(startData, data);
            Assert.AreEqual(startData2, data);

            startInt = 0;
            this.broadcasterService.Invoke<int>(this.firstBroadcaster, 5);
            Assert.AreEqual(startInt, 5);

            this.broadcasterService.Invoke<int>(this.firstBroadcaster, 5);
            Assert.AreEqual(startInt, 10);
        }

        [Test]
        public void RemoveListeners()
        {
            this.broadcasterService.CreateChannel<DummyBroadcaster1, Data>(this.firstBroadcaster);
            this.broadcasterService.TryJoinChannel<DummyBroadcaster1, Data>(DataListener);
            this.broadcasterService.LeaveChannel<DummyBroadcaster1, Data>(DataListener);
            
            startData = new Data() { x = 1f, y = 1f, z = 1f };
            var data = new Data() { x = 2f, y = 2f, z = 2f };
            this.broadcasterService.Invoke<Data>(this.firstBroadcaster, data);
            Assert.AreNotSame(startData, data);
        }

        private Data startData;
        private Data startData2;
        private int startInt;
        //listener methods
        public void DataListener(Data newData)
        {
            startData = newData;
        }

        public void OtherDataListener(Data newData)
        {
            startData2 = newData;
        }

        public void AddListener(int number)
        {
            startInt += number;
        }

        public void RandomListener(Data data) { }

    }
}