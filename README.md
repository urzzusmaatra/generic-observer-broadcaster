# README #

### What is this repository for? ###

* This repo contais observer pattern mixed with service that distributes delegates from observers to subjects without anoyone knowing about the other. It's used to plug-in as dll and used as a service.
* v1 (probably will stay like that)

### How do I get set up? ###

Download repo and use visual studio. Build broadcaster solution for usable dll. BroadcasterTest solution uses NUnit framework for testing (NUnit with NUnit3TestAdapter). It uses debug version of dll for testing.

### How to use Broadcaster ###
Use `DataBroadcasterService` as a service, so its instance should be avaliable to subjects and observers.

`CreateChannel<SubjectType, DataPassingType>(SubjectType subjectInstance)` creates a channel for specific subject, and adds one type of prodcast with specified data type.

`TryJoinChannel<SubjectType, DataPassingType>(Action<DataPassingType> action)` is for observer to join an existing channel. Returns true if joined.

`LeaveChannel` works simmilar to `TryJoinChannel`.

`ContainsChannel<SubjectType>()` returns true if channel of type exists.

`Invoke<DataPassingType>(Object invoker, DataPassingType data)` is used by subjects to invoke action to attached observers.

### Licence ###

    Copyright (C) 2020 Milos Ristic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    See <http://www.gnu.org/licenses/>.